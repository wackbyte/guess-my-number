use {
    rand::Rng,
    std::io::{self, stdin},
};

const MIN: i32 = 1;
const MAX: i32 = 10;

fn input() -> io::Result<String> {
    let mut buffer = String::new();
    stdin().read_line(&mut buffer)?;
    Ok(buffer.trim().to_string())
}

fn guess() -> io::Result<i32> {
    loop {
        match input()?.parse::<i32>() {
            Ok(guess) => break Ok(guess),
            Err(_) => {
                println!("That's not a number!");
            }
        }
    }
}

fn main() -> io::Result<()> {
    let mut rng = rand::thread_rng();

    'outer: loop {
        let rand = rng.gen_range(MIN..=MAX);
        let mut tries: u32 = 0;

        println!("I chose a number {} through {}. What is it?", MIN, MAX);

        loop {
            let guess = guess()?;
            tries += 1;

            if guess > MAX {
                println!("Too high.");
            } else if guess < MIN {
                println!("Too low.");
            } else if guess > rand {
                println!("Lower...");
            } else if guess < rand {
                println!("Higher...");
            } else if guess == rand {
                println!(
                    "You win! The number was {}. You guessed it in {} {}.",
                    rand,
                    tries,
                    if tries == 1 { "try" } else { "tries" },
                );
                break;
            }
        }

        println!("Want to play again? (y/n)");
        'inner: loop {
            match input()?.to_lowercase().as_str() {
                "y" | "ye" | "yes" | "sure" | "yass" => {
                    println!("Okay.");
                    break 'inner;
                }
                "n" | "no" | "go away" => {
                    println!("Goodbye!");
                    break 'outer;
                }
                "sus" => {
                    println!("No. Stop it.");
                }
                _ => {
                    println!("Huh? What?");
                }
            }
        }
    }

    Ok(())
}
